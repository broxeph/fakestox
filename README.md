# FakeStox

Simulate stock trades! Now, with 100% less risk!

Homepage: https://gitlab.com/aball/fakestox/

This project was forked from a previous Django-powered project of mine, but we're doing things Functional Style this round, since it's high time I got my hands dirty with this OTP/GenServer magic I've been reading about.

After grokking Django pretty thoroughly in my day job, it was either FastAPI or Phoenix on deck, and the latter (functional paradigm) seems more interesting and challenging.

That, and LiveView looks really cool as an alternative to the complexity of React/Vue.

Together with distributed SQLite on fly.io via Litestream, this is looking pretty Hipster Stack :)

> SQLite in prod...? What about scaling and durability?

https://litestream.io/blog/why-i-built-litestream/

## Run the Trap

- TODO

## Stack

- Backend
    - SQLite  # TODO
    - Elixir  # TODO
    - Phoenix  # TODO
- Frontend
    - LiveView  # TODO
- CI
    - GitLab CI  # TODO
- CD
    - GitLab CD  # TODO
- Infra
    - fly.io  # TODO

## Useful Commands

- TODO

## Resources

- TODO
